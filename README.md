# test-cicd-phx

This is a test project of CI/CD with Phoenix Framework of Elixir

## Setup Environment

  ### Install Erlang & Elixir with asdf

    .tool-version

    & asdf install

  ### Install Phoenix framework

    mix archive.install hex phx_new

## Phoenix Guide's [hello] Project 

  https://hexdocs.pm/phoenix/up_and_running.html

  $ mix phx.new hello --database mysql

  ### Create docker-compose.yml to setup a MySql Local Server

    hello/config/dev.exs - config :hello, Hello.Repo,

  $ docker-compose up

  $ mix phx.server


###### end of line